import { Todo } from './todo';

export enum DateSearchFilterType {
  Exact = "EXACT",
  Before = "BEFORE",
  After = "AFTER",
}

export interface DateSearchFilter {
  type: DateSearchFilterType;
  timestamp: number;
}

export interface TodosSearchFilters {
  /**
   * String to use for text search within the Todos text fields
   */
  text?: string;

  /**
   * Filter Todos based 
   */
  completed?: boolean;

  dueDate?: DateSearchFilter;
  completedDate?: DateSearchFilter;
}

interface TodosAppDataContext {
  getAllTodos(filters?: TodosSearchFilters): Promise<Todo[]>;
  getOneTodo(id: string): Promise<Todo | undefined>
}

export {
  TodosAppDataContext,
  Todo
}
