import { TodosAppDataContext, Todo, TodosSearchFilters, DateSearchFilter } from '../index';
import * as faker from 'faker';
import { v4 as uuidv4 } from 'uuid';
import MiniSearch from 'minisearch'
import { toUnicode } from 'punycode';

function createRandomTodo(): Todo {
  const dueDate = faker.date.future();
  const completed = faker.random.boolean();
  const completedDate = completed ? faker.date.future(1, dueDate) : null;

  return {
    id: uuidv4(),
    title: faker.lorem.sentence(),
    description: faker.lorem.paragraphs(2),
    completed,
    dueDate: dueDate.getTime(),
    completedDate: completedDate?.getTime()
  };
}

export default class MemoryDataContext implements TodosAppDataContext {
  private todos: Todo[];
  private miniSearch: MiniSearch;

  public constructor() {
    // Generate random Todos
    this.todos = new Array<Todo>(30);
    for (let i = 0; i < this.todos.length; i++) {
      this.todos[i] = createRandomTodo();
    }

    // Create text search index
    this.miniSearch = new MiniSearch({
      fields: [ 'title', 'description' ],
      searchOptions: {
        boost: { title: 2 },
      }
    });
    this.miniSearch.addAll(this.todos);
  }

  private performTextSearch(phrase: string): Todo[] {
    const ids = this.miniSearch.search(phrase).slice(0, 10).map(sr => sr.id);
    console.log(ids);
    // Re map todo entity with id returned from text search operation
    return this.todos.filter(todo => ids.includes(todo.id));
  }

  public async getAllTodos(filters?: TodosSearchFilters): Promise<Todo[]> {
    let result = this.todos;
    if (filters) {
      if (filters.text) {
        result = this.performTextSearch(filters.text);
      }
      if (filters.completed !== undefined) {
        result = result.filter(r => r.completed === filters.completed);
      }
      if (filters.dueDate) {

      }
    }

    return result;
  }

  public async getOneTodo(id: string): Promise<Todo | undefined> {
    return this.todos.find((todo) => todo.id === id);
  }
}
