import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import session from 'express-session';
import cors from 'cors';
import * as createError from 'http-errors';
import asyncHandler from 'express-async-handler';

import * as dotenv from 'dotenv';
import * as middlewares from './middlewares';

import MemoryDataContext from './models/concretions/memoryDataContext'
import { TodosController } from './controllers';

(async () => {
  dotenv.config({ debug: true });

  const app = express();

  app.use(cors());
  app.use(bodyParser.json());

  // Here, we could choose to instantiate a data context concretion that's tied to a database
  // to inject in the controller instances
  const memDataCtx = new MemoryDataContext();

  const todosController = new TodosController(memDataCtx);

  app.use('/todos/:todoId', asyncHandler(todosController.handleGetOneTodo.bind(todosController)));
  app.use('/todos', asyncHandler(todosController.handleGetAllTodos.bind(todosController)));
  // eslint-disable-next-line no-console
  app.use((req) => { console.log(`404 For URL ${req.url}`); throw new createError.NotFound(); });
  app.use(middlewares.ErrorMiddleware.defaultErrorHandler);

  const PORT = process.env.PORT || 8000;
  app.listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`server started at http://localhost:${PORT}`);
  });
})().catch((e) => {
  // eslint-disable-next-line no-console
  console.log('Error made its way to root level: ', e);
});
