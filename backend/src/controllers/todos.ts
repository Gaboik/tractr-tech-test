import { Request, Response, NextFunction } from 'express';
import * as createError from 'http-errors';
import BaseController from './base';
import { TodosAppDataContext, TodosSearchFilters } from '../models/index';
import HttpStatusCode from './helpers/statusCodes';

export default class TodosController extends BaseController {
  constructor(ctx: TodosAppDataContext) {
    super(ctx);
  }

  async handleGetAllTodos(req: Request, res: Response, next: NextFunction): Promise<void> {
    console.log(req.query);

    const filters: TodosSearchFilters = {
      text: req.query?.searchQuery?.toString().trim(),
      completed: req.query?.completed === 'true' || undefined
    }
    const allTodos = await this.dataContext.getAllTodos(filters);
    res.send(allTodos);
  }

  async handleGetOneTodo(req: Request, res: Response, next: NextFunction): Promise<void> {
    const todoId = req.params.todoId;
    if (todoId) {
      const todo = await this.dataContext.getOneTodo(todoId);

      if (todo) {
        res.send(todo);
      } else {
        throw new createError.NotFound();
      }
    }

    throw new createError.NotFound();
  }
}
