import { TodosAppDataContext } from '../models';

export default class BaseController {
  protected dataContext: TodosAppDataContext;

  constructor(ctx: TodosAppDataContext) {
    this.dataContext = ctx;
  }
}
