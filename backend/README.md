# Getting started
Simply run `npm i` to install dependencies
Run the application with `npm run start`

# How it's built
This server is build using NodeJS (duh) and Express.

The codebase is structured to maximize maintainability by promoting loose coupling between the different components.
For example, the 'data context' is injected into the Controllers as a dependency to their conctructors.
This allows the application startup logic to inject different implementation of the 'data context' depending on, for instance, the current environment (dev or prod).
This also makes for very easy separate unit testing of both the controllers and the 'data context' implementations.

## The API
The API returns 'Todo' entities.

The API has 2 routes:
- `/todos`
Returns all todos from the data context

Takes filter as query parameters

- `/todos/:todoId`
Returns one todo with the matching ID

## CORS
For the purposes of this demo, the API allows CORS from any domain.

# Shortcomings

- The data is not persisted across server restarts, although, since the data context is injected into the controllers, adding a data persistence mechanism, a.k.a. a database would be fairly trivial.

- Simpl because of how Express.JS is designed, the code contained in the controllers is not totally type safe; in the sense that, for example, in the `TodosController#handleGetOneTodo` method, we're forced to check that the `todoId` parameter exists to avoid runtime errors. This however, is inconsistent with the semantics of an HTTP API because, well, if the route matched in the first place, it means that there is indeed a value that corresponds to the `todoId` parameter, but Express is unable to guarantee that via the typescript type system.

- The server doesn't use a timezone to encode the date fields on the `Todo` entities, ultimately this coould lead to some bugs for clients in different time zones
