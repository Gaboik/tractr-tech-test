module.exports = {
    plugins: [
      '@typescript-eslint',
      'eslint-comments',
    ],
    extends: [
      'airbnb-typescript/base',
      'plugin:@typescript-eslint/recommended',
      'plugin:eslint-comments/recommended',
    ],
    parserOptions: {
      project: './tsconfig.eslint.json',
    },
    rules: {
      'import/prefer-default-export': 'off',
    },
  };
  