import envVars from 'preact-cli-plugin-env-vars';
import * as Webpack from 'webpack';

/**
 * Attempts to find the CssLoader plugin config object within a webpack
 * modules configuration object
 * @param {object} rules The webpack config rules object
 * @returns { object | null } The CssLoader config object, if found
 */
function findCssLoaderMiddleware(rules) {
  for (const rule of rules) {
    if (rule.use) {
      for (const cfg of rule.use) {
        if (cfg.loader && cfg.loader.split('/').includes("css-loader")) {
          console.log('cfg', cfg);
          return cfg
        }
      }
    }
  }

  return null;
}

export default (config, env, helpers) => {
  envVars(config, env, helpers);

  if (env.isProd) {
    config.devtool = false;
  }

  const cssLoader = findCssLoaderMiddleware(config.module.rules);
  // Make it so LESS is compiled to CSS without adding the
  // '__[hash]' suffix
  cssLoader.options.modules.localIdentName = '[local]';

  config.devtool = "eval-cheap-module-source-map";
}
