# Todos app starter

Template Preact app

## CLI Commands
*   `npm run dev`: Run development server

# How it's built

This Preact application is built with maintanability in mind.
The MobX stores are injected as a dependency to the components of the application via a Preact Context.
This allows the implementation of the stores to be unit tested individually and to be swapped out, if need be.

The stores themselves rely on an injected concretion of a generic interface that describes the data model of the application.
This makes it so that different implementations that abstract away the transport layer can be injected according to the use case.
For example, an "in RAM" service might be injected to display the component in a tool like Storybook.
