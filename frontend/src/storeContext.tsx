/** @jsx h */
import Preact, { FunctionalComponent, createContext, h } from "preact";
import { TodoAppService } from "./services/TodoAppService";
import { RootStore } from "./stores/rootStore";
import { TodoStore } from './stores/todoStore';
import { useContext } from 'preact/hooks';

/**
 * Represents the application data structure
 */
export interface ApplicationDataModel {
  /**
   * Stores data that is related to the application domain
   */
  rootStore: RootStore;

  todoStore: TodoStore;
}

/**
 * Props necessary to instantiate a StoreContextProvider
 */
export interface StoreComponentProps {
  children: Preact.ComponentChildren;
  /**
   * An implementation of `TodoAppService`
   * to be consumed by the data stores
   */
  todoService: TodoAppService;
}

let _storeContext: Preact.Context<ApplicationDataModel>;

export const useStores = () => {
  const store = useContext(_storeContext)

  if (!store) {
    // this is especially useful in TypeScript so the clients don't need to be checking for null all the time
    throw new Error('useStore must be used within a StoreProvider.')
  }

  return store;
}

export const StoreContextProvider: FunctionalComponent<StoreComponentProps> = ({
  children, todoService: todoService
}) => {
  const rootStore = new RootStore(todoService);

  const todoStore = rootStore.todoStore;

  const StoreContext = createContext<ApplicationDataModel>({
    rootStore, todoStore
  });
  _storeContext = StoreContext;

  const stores = {
    rootStore,
    todoStore: rootStore.todoStore
  };
  return (
    <StoreContext.Provider value={stores}>
      {children}
    </StoreContext.Provider>
  );
}
