import { Todo } from "../../models";
import { TodoAppService, TodoFilters } from "../../TodoAppService";
import axios, { AxiosInstance } from 'axios';

class TodoHttpService implements TodoAppService {
  private axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: 'http://localhost:8000/'
    });
  }

  async getAllTodos(filters: TodoFilters | undefined): Promise<Todo[]> {
    const res = await this.axiosInstance.get('/todos', {
      params: filters
    });
    return res.data;
  }

  async getTodoById(id: string): Promise<Todo | null> {
    const res = await this.axiosInstance.get(`/todos/${id}`);
    return res.data || null;
  }
}

export default new TodoHttpService();
