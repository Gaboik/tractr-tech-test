export interface Todo {
  /**
   * Unique identifier for the Todo
   */
   id: string;

   title: string;
 
   /**
    * Arbitrary user-generated description
    */
   description: string;
 
   /**
    * Timestamp of the date at which the Todo is due to be completed
    */
   dueDate: number;
 
   /**
    * Whether or not the Todo has been completed
    */
   completed: boolean;
 
   /**
    * Date at which the Todo was completed, if it was completed
    */
   completedDate?: number;
}
