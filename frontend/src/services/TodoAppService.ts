import { Todo } from "./models";

export interface TodoFilters {
  searchQuery?: string;
  completed?: boolean;
}

/**
 * Specified the behavior of a Todo service
 */
export interface TodoAppService {
  getAllTodos(filters: TodoFilters | undefined): Promise<Todo[]>;
  getTodoById(id: string): Promise<Todo | null>;
}
