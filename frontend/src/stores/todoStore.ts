import { Todo } from '../services/models';
import { observable, action, makeObservable, runInAction, makeAutoObservable } from 'mobx';
import { RootStore } from './rootStore';
import { TodoAppService, TodoFilters } from '../services/TodoAppService';

export class TodoStore {
  @observable todos: Todo[] = [];
  @observable detailedTodo: Todo | null = null;

  constructor(private root: RootStore, private service: TodoAppService) {
    makeAutoObservable(this, {
      todos: observable
    });
  }

  async getAllTodos(filters: TodoFilters | undefined) {
    const todos = await this.service.getAllTodos(filters);
    runInAction(() => {
      this.todos = todos;
    });
  }

  async getTodoById(id: string) {
    const todo = await this.service.getTodoById(id);
    if (todo) {
      const existingIndex = this.todos.findIndex(t => { t.id === todo.id });
      if (existingIndex >= 0) {
        runInAction(() => {
          this.todos[existingIndex] = todo;
        });
      } else {
        runInAction(() => {
          this.todos.unshift(todo);
        });
      }

      runInAction(() => {
        this.detailedTodo = todo;
      });
    }
  }
}

export default TodoStore;
