import { TodoStore } from './todoStore';
import { TodoAppService } from '../services/TodoAppService';

export class RootStore {
  public todoStore: TodoStore;

  constructor(public service: TodoAppService) {
    this.todoStore = new TodoStore(this, service);
  }
}
