import { FunctionalComponent, Fragment, h } from "preact";
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import { observer } from 'mobx-react';
import { IntlProvider } from 'preact-i18n';

import { StoreContextProvider, useStores } from "../storeContext";
import HttpTodoService from "../services/implementations/HTTP";

import HomePage from '../routes/home/home';
import TodoDetailPage from "../routes/tododetails/tododetails";
import NotFoundPage from '../routes/notfound/notFound';

import Header from "./header/header";
import "./style.less"

const App: FunctionalComponent = () => {
  return (
    <div id="preact_root">
      <StoreContextProvider
        todoService={HttpTodoService}
      >
          <Router>
            <div id="app-body">
              <Header />

              <main class="view">
                <Switch>
                  <Route exact path="/todos/:todoId">
                    <TodoDetailPage />
                  </Route>

                  <Route exact path="/" component={HomePage} />

                  <Route component={NotFoundPage} />
                </Switch>
              </main>
            </div>
          </Router>
      </StoreContextProvider>
    </div>
  );
};

export default App;
