/** @jsx h */
import { FunctionalComponent, h, JSX } from 'preact';
import "./style.less"

export type ButtonProps = {
    onClick?: JSX.MouseEventHandler<HTMLButtonElement>;
    disabled?: boolean;
}

const Button: FunctionalComponent<ButtonProps> = (props) => {
    return (
        <button onClick={props.onClick} disabled={!!props.disabled}>
            {props.children}
        </button>
    )
};

export default Button;
