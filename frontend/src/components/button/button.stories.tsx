/** @jsx h */
import { h } from 'preact';
import { Story } from "@storybook/preact";
import Button, { ButtonProps } from './button';
import '../style.less'

export default {
  title: 'Interactives/Button',
  component: Button
};

type ButtonStoryProps = ButtonProps & {
  text: string;
}

const Template: Story<ButtonStoryProps> = (args) => (
  <div>
    <p>Button</p>
    <p>A thing you {args.disabled ? "can't" : "can"} click on</p>
    <Button {...args}>
      {args.text}  
    </Button>
  </div>
);

export const Normal = Template.bind({});
Normal.args = {
  text: "Click me",
  disabled: false
}


export const Disabled = Template.bind({});
Disabled.args = {
  text: "You can't click me",
  disabled: true
}