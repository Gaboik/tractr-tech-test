/** @jsx h */
import { h, FunctionalComponent } from 'preact';
import { observer } from 'mobx-react';
import { NavLink } from 'react-router-dom';
import "./style.less"

const Header: FunctionalComponent = observer(() => {
  return (
    <header>
      <div class="view">
        <div class="site-name">
          <NavLink to="/">
            <i>TODOS</i>
          </NavLink>
        </div>
        <nav>
          <NavLink exact to="/">
            Todos
          </NavLink>
        </nav>

      </div>
    </header>
  );
});

export default Header;
