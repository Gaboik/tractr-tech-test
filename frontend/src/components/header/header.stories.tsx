/** @jsx h */
import { h } from 'preact';
import { Story } from "@storybook/preact";
import Header from './header';
import '../style.less'

export default {
  title: 'Layout/Header',
  component: Header
};

const Template: Story = () => (
  <Header />
);

export const HeaderStory = Template.bind({});
