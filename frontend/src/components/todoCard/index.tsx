import { FunctionalComponent, h } from "preact";
import { Link } from "react-router-dom";
import { Todo } from "../../services/models";
import './style.less';

const TodoCard: FunctionalComponent<Todo> = (props) => {

  return (
    <Link to={`/todos/${props.id}`}>
      <div className="todo-card">
        <h3 className="todo-title">{ props.title }</h3>
        <p className="todo-body">{ props.description }</p>

        <div className="todo-card-footer">
          
        </div>
      </div>
    </Link>
  );
};

export default TodoCard;
