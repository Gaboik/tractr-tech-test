/** @jsx h */
import { observer } from "mobx-react";
import { FunctionalComponent, h } from "preact";
import { useEffect, useState } from "preact/hooks";
import TodoCard from "../../components/todoCard";
import { TodoFilters } from "../../services/TodoAppService";
import { useStores } from "../../storeContext";
import { useForm, SubmitHandler } from "react-hook-form";

const ListFilters: FunctionalComponent<{
  onFiltersChange: (filters: TodoFilters) => void
}> = (props) => {
  const [ currentFilters, setCurrentFilters ] = useState<TodoFilters>({})

  const handleSearchQueryChanged: h.JSX.GenericEventHandler<HTMLInputElement> = (e) => {
    setCurrentFilters({ ...currentFilters, searchQuery: e.currentTarget.value || undefined });
  }
  const handleCompletedChanged: h.JSX.GenericEventHandler<HTMLInputElement> = (e) => {
    console.log(e.currentTarget.checked);
    setCurrentFilters({ ...currentFilters, completed: e.currentTarget.checked || undefined });
  }

  useEffect(() => {
    props.onFiltersChange(currentFilters);
  }, [ currentFilters ])

  return (
    <div id="todo-list-filters">
      <form onSubmit={e => { e.preventDefault(); }}>
        <input type="text" id="search-bar" name="searchQuery"
          placeholder="Search todos" onChange={handleSearchQueryChanged} />
        <input type="checkbox" onChange={handleCompletedChanged} name="completed" label="completed" />
      </form>
    </div>
  );
}

const HomePage: FunctionalComponent = observer(() => {
  const { todoStore } = useStores();

  useEffect(() => {
    todoStore.getAllTodos(undefined);
  }, []);

  return (
    <div id="home-page">
      <h1>
        Todos
      </h1>
      <ListFilters onFiltersChange={(filters) => { todoStore.getAllTodos(filters) }} />
      <div id="todos-list">
        {todoStore.todos && todoStore.todos.length ? (
          todoStore.todos.map(todo => (<TodoCard {...todo} />))
        ) : (
          <p>No todos to show</p>
        )}
      </div>
    </div>
  );
});

export default HomePage;
