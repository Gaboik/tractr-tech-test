import HomePage from "./home/home";
import NotFoundPage from "./notfound/notFound";

export {
  HomePage, NotFoundPage
};
