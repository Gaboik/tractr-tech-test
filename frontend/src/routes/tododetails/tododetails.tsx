/** @jsx h */
import { observer } from "mobx-react-lite";
import { Fragment, FunctionalComponent, h } from "preact";
import { useEffect } from "preact/hooks";
import { useParams } from "react-router";
import { useStores } from "../../storeContext";

const TodoDetailPage: FunctionalComponent = observer(() => {
  const { todoStore } = useStores();
  const { todoId } = useParams<{todoId: string}>();

  useEffect(() => {
    todoStore.getTodoById(todoId);
  }, []);

  return (
    <div id="todo-details-page">
      <h1>Todo Details</h1>
      { todoStore.detailedTodo ? (
        <Fragment>
          <h2>{ todoStore.detailedTodo.title }</h2>
          <div id="todo-attributes">
            <div className="todo-attribute">
              <span><b>Completed: </b></span>
              <span>{ todoStore.detailedTodo.completed.toString() }</span>
            </div>
            <div className="todo-attribute">
              <span><b>Due date: </b></span>
              <span>{ new Date(todoStore.detailedTodo.dueDate).toLocaleDateString() }</span>
            </div>
          </div>
          <p>{ todoStore.detailedTodo.description }</p>
        </Fragment>
      ) : (
        <p>Todo not found</p>
      ) }
    </div>
  );
});

export default TodoDetailPage;
